import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class MainPage extends StatefulWidget{
  const MainPage({ Key? key }) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Main Page'),
      ),
      body: Center(
        child: MaterialButton(
          height: 50,
          minWidth: 300,
          color: Colors.green,
          child: Text('Test Connection'),
          onPressed: () async {
            // send http request to server and get response
            // if response is ok, print 'ok'
            // else print 'error'
            var url = Uri.https('www.googleapis.com', '/books/v1/volumes', {'q': '{http}'});
            var response = await http.get(url);
            if (response.statusCode == 200) {
              print('ok');
            } else {
              print('error');
            }
          },
        ),
      ),
    );
  }
}